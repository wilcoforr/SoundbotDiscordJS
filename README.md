# A Discord Soundboard Bot in Discord.js #

Made 2018

A discord bot that can play sounds. Also includes a primitive HTML front-end that you can simply launch in your default browser to issue commands (once set up of course).
No fancy webhost needed!

Using modern-ish ES6 Javascript, node.js, opus (for playing sounds), and Discord.js


## Installation instructions ##


1: Install Node.js, get version 8 or higher.

2: Install Python 2.7, into a folder called "Python" (example: C:\Python\) and make sure you set a Path variable there

STEP 2 IS IMPORTANT! Because npm wants to explicitly use Python as the folder for Python 2.7 for building opus

3: Clone or download and unzip this repo into a folder of your choice

4: In that folder, where `sound-bot.js` is located, run `npm install discord.js --save`

5: Create and add/invite your bot to your server - https://discordapp.com/developers/applications/me - use the Discord Dev help or your favorite search engine.

- https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token is a great guide, but not sure how long this will last/stay up


## Commands ##

Once started with `node sound-bot.js`, the bot should join the server for the token specified in your `sound-bot-config.json`.

!help will list all valid commands in the `sound-bot.js` sounds variable (a dictionary data type)

With the default clone/download of this repo, once your bot is started, join a voice channel on your sever, and type `!summon` to summon the bot into your channel.

After the bot has joined the channel, type `!emma` into the text channel that you specified for your bot. A dog barking sound should play, and stop.


## Adding more sounds ##

Add sounds to the `sounds\` folder, and then add that sound command to the `sounds` dictionary variable in `sound-bot.js`

For example, let's pretend that `emma.wav` didn't exist as a command or in the `sounds\` folder yet.

Add `emma.wav` into the `sounds\` folder.

Then, edit the `sounds` variable in `sound-bot.js`, by adding a new dictionary item like so:

`  "!emma" : "emma.wav" , `

Before:

```js
const sounds = {

}
```

After

```js
const sounds = {
    "!emma" : "emma.wav",
}
```

More general:

` "!<your_command_here>" : "<file_name_of_sound.file_extension>", ` !Dont forget the comma `,` at the end


## Screenshot examples ##

### Command ###

gif or vid of !summon, playing a sound, then !dismiss


### Primitive Soundboard ###

![Soundboard Example](https://gitlab.com/wilcoforr/SoundbotDiscordJS/raw/master/soundboard-html-example.jpg)

Press a button that sends a command to your discord server, that will play a sound. You'll need to make a seperate app and bot for this.

Read the source code of the HTML file to get an idea on how to use this basic soundboard.


