//Brushing up on modern JavaScript
//Made in Discord.js 11.3+
//Node v0.8.1

// Discord JS license:
// Apache License
// Version 2.0, January 2004
// http://www.apache.org/licenses/

//References/Docs:
//https://discord.js.org/#/docs/main/stable/general/welcome

//Instructions (read the README.md for more finer detail)
//0. Create a discord bot and invite it to your server, set DISCORD_TOKEN with the bot's Discord Token.
//1: Install Python 2.7 and add it to path, along with ffmpeg
//2: `npm install discord.js node-opus --save`
//3: Start with `node sound-bot.js`
//4: `!summon` the bot while in a voice channel, and try out any of the default sounds (!emma, !johncena, etc)


//Usage: you and other users join a voice channel, summon the bot via `!summon`
//       then trigger sounds to playFile by issuing a valid sounds command in 
//       the `sounds` dictionary.
//       for example: !emma
//       should play a dog barking sound [emma is a dog :) ]

//Add sounds here. 
//This is a dictionary that represents the key, and the value.
//The key is a unique command, and the value is the filename of the 
//sound to be played that is located in the `sounds\` folder.
//Make sure to add a comma (,) after each item.
const sounds = {
    "!emma" : "emma.wav",

    "!johncena" : "johncena.webm",
    
    //the key is always unique - thus you can use aliases for a command like !mgsalert and !alert shown here to display the same sound file
    "!mgsalert" : "mgsalert.webm",
    "!alert" : "mgsalert.webm", 


    //"!command" : "filename.ext", 
    //above is a template, copy, paste, uncomment, add your sound.
}

const SOUNDS_DIR = "./sounds/";
const DISCORD_TOKEN = "your token here";
const SOUND_VOLUME = 1.0; //1.0 == 100% volume, regular, default.
// so that no one can spam sounds for your bot or tell it to keep leaving/!stop command etc
const USE_ADMIN_FEATURES = false; 

const Discord = require("discord.js");
const bot = new Discord.Client();

if (DISCORD_TOKEN === "your token here") {
    logErrors(["Need to set DISCORD_TOKEN!"]);
    process.exit();
}

bot.login(DISCORD_TOKEN);

const soundBotAdminDiscordIDs = [
    "your discord user ID"
    ,"another user ID"
    ,"Looks like a number below:"
    ,"141135273888281211"
]

function isUserASoundBotAdmin(userID) {
    return soundBotAdminDiscordIDs.include(userID);
}


//client is ready to play a sound
var isReady = true;

//voiceChannel is assigned via the !summon command
var voiceChannel;

//textChannel is used for printing out messages
var textChannel;

/*** bot events ***/
//Event emitted when the bot starts up
bot.on("ready", ready => {
    logConsole(banner);
    logConsole("Started Discord Sound Bot!\n\n");
    

});

//Reply to general channel message events, where the bot was invited to via the Token.
//Remember, message is an object, not a string. https://discord.js.org/#/docs/main/stable/class/Message
bot.on("message", message => {

    const msg = message.content.toLowerCase().trim();
    
    //not a valid command, so just return.
    if (msg.indexOf("!") !== 0) {
        return;
    }

    const msgAuthorName = message.author.username;

    textChannel = message.channel;

    if (msg === "!summon") {
        voiceChannel = message.member.voiceChannel;
        voiceChannel.join();//then(success, err).catch(err) //TODO or whatever sytax. add some logging here on the join() promise
    }
    else if (msg === "!dismiss") {
        voiceChannel.leave();
        voiceChannel = undefined;
    }
    //for stopping sounds being played. Primitive, basically exits and rejoins the voice channel.
    else if (msg === "!stopvoice" || msg === "!stop" || !msg === "!reset") {
        if (USE_ADMIN_FEATURES && !isUserASoundBotAdmin(message.author.id)) {
            sendTextMessage("You are not a Soundbot admin and cannot stop this bot.");
            return;
        }
        voiceChannel.leave();
        voiceChannel.join();
    }
    else if (msg === "!kill") {
        if (USE_ADMIN_FEATURES && !isUserASoundBotAdmin(message.author.id)) {
            sendTextMessage("You are not a Soundbot admin and cannot stop this bot.");
            return;
        }
        const killMsg = "Trying to stop sound-bot.js and terminate this process...";
        sendTextMessage(killMsg);
        logConsole(killMsg);
        process.exit();
    }
    //lock for sounds
    //if a sound is not playing and the !<sound-name> is in the `sounds` dictionary, play the sound
    else if (isReady && msg in sounds) { 
        logConsole(msgAuthorName + " wanted to play sound: " + msg);
        playSound(msg);
    }
    else if (msg === "!help") {
        displayHelpText();
    }
});


bot.on("disconnect", disconnect => {
    logConsole("Disconnecting. Bye!");
    process.exit();
});

bot.on("error", error => {
    logErrors(["A Discord.Client() error occurred!", error]);
});

/*** end bot events ***/

//for converting from Promises to async await
// async function playSoundAsync(msg) {
    // var result = await voiceChannel.connection.playFile(SOUNDS_DIR + sounds[msg]);
// }

//Play a sound to the voice channel
//@param msg = string, this is the command to play, eg: "!emma"
function playSound(msg) {
    if (voiceChannel === undefined) {
        const errorMessage = "SoundBot does not have a voice channel defined!" 
                            + "\nNeed to `!summon` bot when you are in a voice channel in order to play sounds.";
        sendTextMessage(errorMessage);
        logErrors([errorMessage]);
        return;
    }

    isReady = false;
    try {
        //TODO: instead of promise pattern, implement this as async
        //check out: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
        const dispatcher = voiceChannel.connection.playFile(SOUNDS_DIR + sounds[msg], { volume: SOUND_VOLUME });

        dispatcher.on("end", end => {
            logConsole(["Stopped playing sound " + sounds[msg]]);
            isReady = true;
        });

        dispatcher.on("error", error => {
            logErrors(["An error occurred playing the sound with the dispatcher!", error]);
            voiceChannel.leave();
            voiceChannel = undefined;
            isReady = true;
        });
    }
    catch(error) {
        logErrors(["Tried playing a sound", "Command/msg was: " + msg, error]);
        voiceChannel.leave();
        voiceChannel = undefined;
    }
}

//maybe a sound downloading feature in the future. would have to change `consts` into a config file or something on disc to manipulate
// var http = require('http');
// var fs = require('fs');
// function addSoundToDiscFromAttachment(messageAttachment, soundName) {
//     var file = fs.createWriteStream(soundName);
//     var request = http.get(messageAttachment.url, response => {
//         response.pipe(file)
//     });
// }



//send a text message to the desired channel
//@param channel - a Discord text channel
//@param msg - a string of what text to send
function sendTextMessage(msg) {
    if (textChannel !== undefined) {
        textChannel.send(msg);
    }
}


//Display help text
function displayHelpText() {
    let helpText = "Displaying help text for SoundBot Discord.js\n";
    helpText += "`!help` to display this help text again.\n";
    helpText += "`!summon` to summon the bot to the voice channel you are currently in.\n";
    helpText += "`!dismiss` to dismiss the bot from voice channel it is connected to.\n";
    helpText += "`!stop` or `!reset` to stop the current sound or reset the bot.\n";
    helpText += "Below are the valid sound files commands.\n";
    
    for (let key in sounds) {
        if (sounds.hasOwnProperty(key)) {
            helpText += "`" + key + "`" + " will play: " + sounds[key] + "\n";
        }
    }

    sendTextMessage(helpText);
}



//log general info or debug text to the console
function logConsole(msg) {
    const dateStr = getDateString();
    console.log(dateStr + ": " + msg);
}

//Log errors to the console in a red font.
function logErrors(errorsArray) {
    const dateStr = getDateString();
    // https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
    const redConsoleFont = "\x1b[31m";
    errorsArray.forEach(err => {
        console.log(redConsoleFont + dateStr + ": " + err);
    });
    console.log("\x1b[0m"); //reset font to white/default
}

function getDateString() {
    return new Date().toLocaleString();
}


const banner = 
`
\n
    ███████╗ ██████╗ ██╗   ██╗███╗   ██╗██████╗ ██████╗  ██████╗ ████████╗  
    ██╔════╝██╔═══██╗██║   ██║████╗  ██║██╔══██╗██╔══██╗██╔═══██╗╚══██╔══╝  
    ███████╗██║   ██║██║   ██║██╔██╗ ██║██║  ██║██████╔╝██║   ██║   ██║     
    ╚════██║██║   ██║██║   ██║██║╚██╗██║██║  ██║██╔══██╗██║   ██║   ██║     
    ███████║╚██████╔╝╚██████╔╝██║ ╚████║██████╔╝██████╔╝╚██████╔╝   ██║     
    ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚═════╝ ╚═════╝  ╚═════╝    ╚═╝     
                                                                            
    ██████╗ ██╗███████╗ ██████╗ ██████╗ ██████╗ ██████╗         ██╗███████╗ 
    ██╔══██╗██║██╔════╝██╔════╝██╔═══██╗██╔══██╗██╔══██╗        ██║██╔════╝ 
    ██║  ██║██║███████╗██║     ██║   ██║██████╔╝██║  ██║        ██║███████╗ 
    ██║  ██║██║╚════██║██║     ██║   ██║██╔══██╗██║  ██║   ██   ██║╚════██║ 
    ██████╔╝██║███████║╚██████╗╚██████╔╝██║  ██║██████╔╝██╗╚█████╔╝███████║ 
    ╚═════╝ ╚═╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝ ╚════╝ ╚══════╝ 
                                                                        
    by wilcoforr  
    https://gitlab.com/wilcoforr/SoundbotDiscordJS
\n
`
